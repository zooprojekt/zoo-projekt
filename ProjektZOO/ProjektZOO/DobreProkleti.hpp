//
//  DobreProkleti.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef DobreProkleti_hpp
#define DobreProkleti_hpp

#include <stdio.h>
#include "Prokleti.hpp"
class DobreProkleti: public Prokleti
{
    DobreProkleti(string nazev);
    int GetBonus();
    void PrintInfo();
}
#endif /* DobreProkleti_hpp */
