//
//  Hra.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef Hra_hpp
#define Hra_hpp
#include "HerniPlocha.hpp"
#include "Hrac.hpp"
#include <vector>
using namespace std;

#include <stdio.h>
class Hra
{
private:
HerniPlocha* m_plocha;
vector<Hrac*> m_hraci;
public:
    Hra(HerniPlocha* plocha);
    void VypisHrace();
    void Konec();
};
#endif /* Hra_hpp */
