//
//  Hrac.cpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#include "Hrac.hpp"
Hrac::Hrac(string jmeno)
{
    
}

void Hrac::VypisHrady()
{
    cout << "Hrady:" << endl;
    for (int i=0; i<m_hrady.size(); i++)
    {
        cout << i << ". " << m_hrady.at(i)->GetPopis() << endl;
    }
}

void Hrac::VypisSutry()
{
    cout << "Hrady:" << endl;
    for (int i=0; i<m_sutry.size(); i++)
    {
        cout << i << ". " << m_sutry.at(i)->GetPopis() << endl;
    }
    
}

void Hrac::VypisVojska()
{
    cout << "Hrady:" << endl;
    for (int i=0; i<m_vojska.size(); i++)
    {
        cout << i << ". " << m_vojska.at(i)->GetPopis() << endl;
    }
    
}

void Hrac::VypisPrikazy()
{
    cout << "Hrady:" << endl;
    for (int i=0; i<m_prikazy.size(); i++)
    {
        cout << i << ". " << m_prikazy.at(i)->GetPopis() << endl;
    }
    
}

void Hrac::NaucPrikaz(Prikaz* prikaz)
{
    m_prikazy.push_back(prikaz);
}


string Hrac::GetJmeno()
{
    return m_jmeno;
}

void Hrac::Prikaz()
{
    int ktereVojsko = 0;
    VypisVojska();
    cout << "Kteremu vojsku chces prikazat?" << endl;
    cin >> ktereVojsko;
    
    int kteryPrikaz = 0;
    VypisPrikazy();
    cout << "Co chces vojsku prikazat?" << endl;
    cin >> kteryPrikaz;
    
    m_prikazy.at(kteryPrikaz)->PrikazVojsku(m_vojska.at(ktereVojsko));
    
}

void Hrac::ZiskejVojsko(Vojsko* ktere)
{
    m_vojska.push_back(ktere);
}

void Hrac::ZiskejHrad(Hrad* ktery)
{
    m_hrady.push_back(ktery);
}

