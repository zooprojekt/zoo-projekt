//
//  Hrac.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef Hrac_hpp
#define Hrac_hpp

#include <stdio.h>
#include <iostream>
#include "Sutry.hpp"
#include "Vojsko.hpp"
#include "Hrad.hpp"
#include "Prikaz.hpp"
#include "Prokleti.hpp"
#include <vector>
using namespace std;

class Hrac
{
private:
    int m_chleba;
    string m_jmeno;
    vector<Sutry*> m_sutry;
    vector<Hrad*> m_hrady;
    vector<Vojsko*> m_vojska;
    vector<Prikaz*> m_prikazy;
    vector<Prokleti*> m_prokleti;
public:
    Hrac(string jmeno);
    void VypisHrady();
    void VypisVojska();
    void VypisPrikazy();
    void ZiskejVojsko(Vojsko* ktere);
    void ZiskejHrad(Hrad* ktery);
    void NaucPrikaz(Prikaz* prikaz);
    void ZaklejSe();
    void VypisSutry();
    string GetJmeno();
    void Prikaz();
};

#endif /* Hrac_hpp */
