//
//  Hrad.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef Hrad_hpp
#define Hrad_hpp

#include <stdio.h>
#include <iostream>
#include "Sutry.hpp"
using namespace std;
class Hrad
{
private:
    int m_obrana;
public:
    Hrad(int obrana);
    void ZvysObranu(Sutry* sutr);
    string GetPopis();
    
};
#endif /* Hrad_hpp */
