//
//  Pocestni.cpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#include "Pocestni.hpp"
Pocestni::Pocestni(int sila, int hlad)
{
    m_sila = sila;
    m_hlad = hlad;
}

int Pocestni::GetSila()
{
    return m_sila;
}

int Pocestni::GetHlad()
{
    return m_hlad;
}
