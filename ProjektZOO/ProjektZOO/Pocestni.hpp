//
//  Pocestni.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef Pocestni_hpp
#define Pocestni_hpp

#include <stdio.h>

class Pocestni
{
private:
    int m_sila;
    int m_hlad;
public:
    Pocestni(int sila, int hlad);
    int GetSila();
    int GetHlad();
};

#endif /* Pocestni_hpp */
