//
//  Prikaz.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef Prikaz_hpp
#define Prikaz_hpp

#include <stdio.h>
#include "Vojsko.hpp"
#include <iostream>
using namespace std;
class Prikaz
{
public:
    virtual void PrikazVojsku(Vojsko* vojsko)=0;
    virtual string GetPopis()=0;
};
#endif /* Prikaz_hpp */
