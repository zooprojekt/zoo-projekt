//
//  PrikazAgressive.cpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#include "PrikazAgressive.hpp"

void PrikazAgressive::PrikazVojsku(Vojsko* vojsko)
{
    vojsko->Agresivne(5, 5);
}

string PrikazDefensive::GetPopis()
{
    return "Zvysi se ti utok, ale snizi obrana.";
}
