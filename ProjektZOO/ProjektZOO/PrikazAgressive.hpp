//
//  PrikazAgressive.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef PrikazAgressive_hpp
#define PrikazAgressive_hpp

#include <stdio.h>
#include <iostream>
#include "Vojsko.hpp"
using namespace std;
class PrikazAgressive: public Prikaz
{
public:
    void PrikazVojsku(Vojsko* vojsko);
    string GetPopis();
    
};
#endif /* PrikazAgressive_hpp */
