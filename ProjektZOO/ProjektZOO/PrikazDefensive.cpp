//
//  PrikazDefensive.cpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#include "PrikazDefensive.hpp"

void PrikazDefensive::PrikazVojsku(Vojsko* vojsko)
{
    vojsko->Defensivne(5, 5);
}

string PrikazDefensive::GetPopis()
{
    return "Zvysi se ti obrana, ale snizi utok.";
}
