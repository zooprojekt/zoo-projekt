//
//  PrikazDefensive.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef PrikazDefensive_hpp
#define PrikazDefensive_hpp

#include <stdio.h>
#include <iostream>
#include "Vojsko.hpp"
using namespace std;
class PrikazDefensive: public Prikaz
{
public:
    void PrikazVojsku(Vojsko* vojsko);
    string GetPopis();
    
};
#endif /* PrikazDefensive_hpp */
