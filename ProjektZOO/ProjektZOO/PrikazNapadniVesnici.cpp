//
//  PrikazNapadniVesnici.cpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#include "PrikazNapadniVesnici.hpp"

void PrikazNapadniVesnici::PrikazVojsku(Vojsko* vojsko)
{
    vojsko->ZmenPovest(-10);
    vojsko->UkradniChleba(10);
}

string PrikazNapadniVesnici::GetPopis()
{
    return "Napadnes bezbranne vesnicany, ukradnes jim jidlo a snizi se ti povest.";
}

