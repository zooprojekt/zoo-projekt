//
//  PrikazNapadniVesnici.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef PrikazNapadniVesnici_hpp
#define PrikazNapadniVesnici_hpp

#include <stdio.h>
#include <iostream>
#include "Vojsko.hpp"
using namespace std;
class PrikazNapadniVesnici: public Prikaz
{
public:
    void PrikazVojsku(Vojsko* vojsko);
    string GetPopis();
    
};
#endif /* PrikazNapadniVesnici_hpp */
