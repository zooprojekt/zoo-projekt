//
//  PrikazPomozVesnicanum.cpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#include "PrikazPomozVesnicanum.hpp"

void PrikazPomozVesnicanum::PrikazVojsku(Vojsko* vojsko)
{
    vojsko->Narukuj(10);
    vojsko->PodelSeOChleba(10);
}

string PrikazPomozVesnicanum::GetPopis()
{
    return "Pomuzes vesnicanum a podelis se s nimi o jidlo, oni ti za to daji mlade muze do boje.";
}
