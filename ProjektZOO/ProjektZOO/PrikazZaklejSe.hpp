//
//  PrikazZaklejSe.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef PrikazZaklejSe_hpp
#define PrikazZaklejSe_hpp

#include <stdio.h>
#include <iostream>
#include "Vojsko.hpp"
using namespace std;
class PrikazZaklejSe: public Prikaz
{
public:
    void PrikazVojsku(Vojsko* vojsko);
    string GetPopis();
    
};
#endif /* PrikazZaklejSe_hpp */
