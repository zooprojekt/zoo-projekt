//
//  Prokleti.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef Prokleti_hpp
#define Prokleti_hpp

#include <stdio.h>
#include <iostream>
using namespace std;

class Prokleti
{
private:
    string m_nazev;
public:
    Prokleti(string nazev);
    virtual int GetBonus() = 0;
    void PrintInfo();
};
#endif /* Prokleti_hpp */
