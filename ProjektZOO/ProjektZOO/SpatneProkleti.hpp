//
//  SpatneProkleti.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef SpatneProkleti_hpp
#define SpatneProkleti_hpp

#include <stdio.h>
#include "Prokleti.hpp"
class SpatneProkleti: public Prokleti
{
    SpatneProkleti(string nazev);
    int GetBonus();
    void PrintInfo();
}
#endif /* SpatneProkleti_hpp */
