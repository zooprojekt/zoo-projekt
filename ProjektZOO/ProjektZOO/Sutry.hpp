//
//  Sutry.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef Sutry_hpp
#define Sutry_hpp

#include <stdio.h>
#include <iostream>
using namespace std;

class Sutry
{
private:
    int m_vaha;
public:
    Sutry(int vaha);
    string GetPopis();
    int GetVaha();
}

#endif /* Sutry_hpp */
