//
//  Vesnice.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef Vesnice_hpp
#define Vesnice_hpp

#include <stdio.h>
#include <iostream>
using namespace std;
class Vesnice
{
private:
    string nazev;

public:
    Vesnice(string nazev);
};
#endif /* Vesnice_hpp */
