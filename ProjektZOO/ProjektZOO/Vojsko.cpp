//
//  Vojsko.cpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 31/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#include "Vojsko.hpp"

Vojsko::Vojsko(int sila, int obrana, int povest, int chleba)
{
    m_sila = sila;
    m_obrana = obrana;
    m_povest = povest;
    m_chleba = chleba;
}

void Vojsko::PotkejPocestneho(Pocestni* pocestny)
{
     m_chleba = m_chleba - (pocestny->GetHlad());
     m_sila = m_sila + (pocestny->GetSila());
}

void Vojsko::SetSila(int sila)
{
    m_sila = sila;
}

void Vojsko::SetObrana(int obrana)
{
    m_obrana = obrana;
}

string Vojsko::GetPopis()
{
    return "Vojsko plne vojaku.";
};

void Vojsko::ZmenPovest(int okolik)
{
    m_povest = m_povest + okolik;
}

void Vojsko::UkradniChleba(int kolik)
{
    m_chleba = m_chleba + kolik;
}

void Vojsko::Narukuj(int kolik)
{
    m_sila = m_sila + kolik;
}

void Vojsko::PodelSeOChleba(int kolik)
{
    m_chleba = m_chleba - kolik;
}

void Vojsko::Agresivne(int agrese, int defensiva)
{
    m_sila = m_sila + agrese;
    m_obrana = m_obrana - defensiva;
}

void Vojsko::Defensivne(int agrese, int defensiva)
{
    m_sila = m_sila - agrese;
    m_obrana = m_obrana + defensiva;
}
