//
//  Vojsko.hpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 31/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#ifndef Vojsko_hpp
#define Vojsko_hpp

#include <stdio.h>
#include "Pocestni.hpp"
#include <iostream>
using namespace std;

class Vojsko
{
private:
    int m_sila;
    int m_obrana;
    int m_chleba;
    int m_povest;
public:
    Vojsko(int sila, int obrana, int povest, int chleba);
    void SetSila(int sila);
    void SetObrana(int obrana);
    void PotkejPocestneho(Pocestni* pocestny);
    string GetPopis();
    void ZmenPovest(int okolik);
    void UkradniChleba(int kolik);
    void Narukuj(int kolik);
    void PodelSeOChleba(int kolik);
    void Defensivne(int agrese, int defensiva);
    void Agresivne(int agrese, int defensiva);

};
#endif /* Vojsko_hpp */
