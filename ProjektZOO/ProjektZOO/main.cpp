//
//  main.cpp
//  ProjektZOO
//
//  Created by Lea Ministrová on 30/12/2018.
//  Copyright © 2018 Lea Ministrová. All rights reserved.
//

#include <iostream>
#include "HerniPlocha.hpp"
#include "Hra.hpp"
#include "Vesnice.hpp"
#include "Hrac.hpp"
int main() {
    
    HerniPlocha* plocha = new HerniPlocha();
    Hra* hra = new Hra(plocha);
    Hrac* hrac = new Hrac("Jarmil");
    
    Vojsko* vojsko1 = new Vojsko(20, 20, 0, 20);
    Vojsko* vojsko2 = new Vojsko(20, 20, 0, 20);
    
    Sutry* sutr1 = new Sutry(10);
    Sutry* sutr2 = new Sutry(10);
    Sutry* sutr3 = new Sutry(10);
    
    Pocestni* pocestny = new Pocestni(5,5);
    
    Hrad* hrad1 = new Hrad(10);
     Hrad* hrad2 = new Hrad(10);
    
    Vesnice* vesnice1 = new Vesnice("Zimohrad");
    Vesnice* vesnice2 = new Vesnice("Přístaviště");
    
    Prikaz* p1 = new PrikazNapadniVesnici();
    Prikaz* p2 = new PrikazPomozVesnicanum();
    Prikaz* p3 = new PrikazDefensive();
    
    hrac->ZiskejVojsko(vojsko1);
    hrac->ZiskejHrad(hrad1);
    hrac->ZiskejHrad(hrad2);
    
    hra->VypisHrace();
    hrac->NaucPrikaz(p1);
    hrac->NaucPrikaz(p2);
    hrac->NaucPrikaz(p3);
    return 0;
};
